FROM debian:buster-slim
MAINTAINER UP Leong <pio.leong@gmail.com>

# Update apt
RUN apt-get update

# Install utilities
RUN apt-get install -y curl git xz-utils unzip make

# Install protoc and protoc-gen-grpc-web
ENV PROTOC_VERSION 3.17.3
RUN curl -L https://github.com/protocolbuffers/protobuf/releases/download/v${PROTOC_VERSION}/protoc-${PROTOC_VERSION}-linux-x86_64.zip -o /tmp/protoc.zip
RUN (cd /usr/local/; unzip /tmp/protoc.zip)
#
RUN curl -L https://github.com/grpc/grpc-web/releases/download/1.2.1/protoc-gen-grpc-web-1.2.1-linux-x86_64 -o /usr/local/bin/protoc-gen-grpc-web && chmod +x /usr/local/bin/protoc-gen-grpc-web

# Install nodejs via NVM
ENV NODE_VERSION 14.2.0
RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.0/install.sh | bash
ENV NVM_DIR /root/.nvm
RUN . $NVM_DIR/nvm.sh && nvm install $NODE_VERSION
ENV NODE_PATH $NVM_DIR/v$NODE_VERSION/lib/node_modules
ENV PATH $NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH
RUN node --version


# Cleanup Apt
RUN apt-get clean
#RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*


# Er, sure, why not
CMD ["/bin/bash"]
