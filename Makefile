all: build

build:
	docker build -t macodigo/debian-nodejs-macodigo .

push:
	docker push macodigo/debian-nodejs-macodigo
